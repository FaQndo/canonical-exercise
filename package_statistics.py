"""
Command line tool that given a Debian architecture (amd64, arm, etc.) and possibly a
Debian repository URL, retrieves and analyzes the Contents index file to print the ten
projects that contain the most files.

It is possible to specify a local file to use as the Contents index, mainly for debugging
of the processing phase.

We do not support the free form text allowed before the table begins.

We do not support the table's column names (namely FILE and LOCATION).

Contents index file encoding is assumed to be UTF-8.
"""

import sys
import collections
import requests
from urllib.parse import urljoin
import gzip
import argparse

PackageEntry = collections.namedtuple('PackageEntry', ['package', 'files_count'])


def get_statistics(lines):
    # We use an OrderedDict so that the order in case of ties is that of
    # appearance in the contents index
    count = collections.OrderedDict()
    for line in lines:
        try:
            # Spaces are allowed in filenames and should be supported. Not so in the
            # package name, so last space indicates beginning of package name.
            last_space = line.rindex(' ')
        except ValueError:
            # There has to be at least one space, between filename and package name, but
            # there's none. Debian says to ignore ill-formed lines.
            continue
        file = line[0:last_space].strip()
        packages = line[last_space:].strip()

        # The package column is actually a packages' list, comma separated.
        packages = packages.split(',')
        for p in packages:
            if p in count:
                count[p] += 1
            else:
                count[p] = 1
    top_ten_items = sorted(count.items(), key=lambda p: p[1], reverse=True)[:10]
    return [PackageEntry(package=p[0], files_count=p[1]) for p in top_ten_items]


def build_url(mirror, arch):
    # Append '/' to the mirror to ensure last segment doesn't get replaced
    return urljoin(mirror + '/', 'Contents-{}.gz'.format(arch))


def main(args):
    if args.local_file:
        file_location = args.local_file
        try:
            with open(args.local_file, 'rb') as f:
                contents_index_gz = f.read()
        except Exception as e:
            print('{}: Error reading file ({}): {}'.format(sys.argv[0], type(e).__name__,
                                                           args.local_file),
                  file=sys.stderr)
            sys.exit(1)
    else:
        url = build_url(args.mirror, args.arch)
        file_location = url
        try:
            response = requests.get(url)
            response.raise_for_status()
        except requests.HTTPError as e:
            print('{}: Error getting URL ({}): {}'.format(sys.argv[0], e, url),
                  file=sys.stderr)
            sys.exit(1)
        except Exception as e:
            print('{}: Error getting URL ({}): {}'.format(sys.argv[0], type(e).__name__,
                                                           url),
                  file=sys.stderr)
            sys.exit(1)
        contents_index_gz = response.content
    try:
        contents_index = gzip.decompress(contents_index_gz).decode('utf-8')
    except Exception as e:
        print('{}: File does not appear to be a gzip file: {}'.format(sys.argv[0],
                                                                      file_location),
              file=sys.stderr)
        sys.exit(1)

    lines = contents_index.split('\n')

    stats = get_statistics(lines)

    longest_package_name = max([len(p.package) for p in stats])
    longest_files_count = len(str(max([p.files_count for p in stats])))
    FMT_STRING = '{{}}. {{:<{}}} {{:>{}}}'.format(longest_package_name,
                                                  longest_files_count)
    TENTH_FMT_STRING = '{{}}. {{:<{}}} {{:>{}}}'.format(longest_package_name - 1,
                                                        longest_files_count)
    for i, p in enumerate(stats):
        fmt = FMT_STRING if i < 9 else TENTH_FMT_STRING
        print(fmt.format(i+1, p.package, p.files_count))


MIRROR = 'http://ftp.uk.debian.org/debian/dists/stable/main/'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=("Print the ten most bloated Debian "
                                                  "packages on a given architecture."))
    parser.add_argument('arch')
    parser.add_argument('-m', '--mirror', action='store', default=MIRROR)
    parser.add_argument('--local-file', action='store',
                        help="Use specified file as the contents index")

    args = parser.parse_args()

    main(args)
