import package_statistics

import gzip
import argparse
import examples

def test_get_statistics():
    stats = package_statistics.get_statistics(examples.CONTENTS_LINES)

    assert len(stats) == 10

    i = 0
    assert stats[i].package == 'utils/bzip2'
    assert stats[i].files_count == 12

    i += 1

    assert stats[i].package == 'utils/coreutils'
    assert stats[i].files_count == 9

    i += 1

    assert stats[i].package == 'admin/btrfs-progs'
    assert stats[i].files_count == 7

    i += 1

    assert stats[i].package == 'admin/libpam-biometric'
    assert stats[i].files_count == 2

    i += 1

    assert stats[i].package == 'admin/brltty'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'admin/fbset'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'admin/fgetty'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'shells/ash'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'shells/bash'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'shells/bash-static'
    assert stats[i].files_count == 1

def test_get_statistics_less_than_ten_pkgs():
    stats = package_statistics.get_statistics(examples.CONTENTS_LESS_THAN_TEN_LINES)

    assert len(stats) == 7

    i = 0
    assert stats[i].package == 'utils/bzip2'
    assert stats[i].files_count == 12

    i += 1

    assert stats[i].package == 'utils/coreutils'
    assert stats[i].files_count == 9

    i += 1

    assert stats[i].package == 'admin/btrfs-progs'
    assert stats[i].files_count == 7

    i += 1

    assert stats[i].package == 'admin/libpam-biometric'
    assert stats[i].files_count == 2

    i += 1

    assert stats[i].package == 'admin/brltty'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'admin/fbset'
    assert stats[i].files_count == 1

    i += 1

    assert stats[i].package == 'admin/fgetty'
    assert stats[i].files_count == 1

def test_build_url():
    assert 'http://the.mirror/Contents-the_arch.gz' == package_statistics.build_url('http://the.mirror/', 'the_arch')
    assert 'http://the.mirror/Contents-the_arch.gz' == package_statistics.build_url('http://the.mirror', 'the_arch')

def test_main(capsys, requests_mock):
    requests_mock.get('http://the.mirror/Contents-amd64.gz',
            content=gzip.compress(examples.CONTENTS_RAW))
    arguments = argparse.Namespace(mirror='http://the.mirror', arch='amd64', local_file=None)
    package_statistics.main(arguments)
    output = capsys.readouterr()
    assert output.out == """1. utils/bzip2            12
2. utils/coreutils         9
3. admin/btrfs-progs       7
4. admin/libpam-biometric  2
5. admin/brltty            1
6. admin/fbset             1
7. admin/fgetty            1
8. shells/ash              1
9. shells/bash             1
10. shells/bash-static     1
"""
